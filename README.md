# Continuous Testing com Jenkins e Ruby

Curso Continuous Testing com Jenkins e Ruby da QANinja!

## Como executar o projeto

* Importante ter o Ruby instalado (versão 2.5 ou superior)

### Instalar o Bundler
'
gem install Bundler
'

### Instalar as dependências do Ruby
'
bundle install
'

### Executar localmente
'
bundle exec cucumber
'

### Executar no servidor de CI (Gerando reports JSON)
'
bundle exec cucumber -p ci
'